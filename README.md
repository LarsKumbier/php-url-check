PHP URL Validator & Job Engine
==============================
Author: Lars Kumbier <lars@kumbier.it>

This is a project consisting of an asynchronous job engine and an url validator


Requirements
============
- php 5.3+
- composer (http://getcomposer.org)
- all other requirements are installed via composer


Installation
============
- clone repository (with --depth=1)
- run `composer update` to install all required files


Usage
=====
Change this file to meet your project


Troubleshooting
===============
Issue Tracking is done on bitbucket


Directory Structure
===================
- `docs/`     Documentation
- `lib/`      Application Libraries
- `tests/`    PHPunit tests
- `tmp/`      A temporary folder, e.g. for code coverage reports
- `vendor/`   automatically filled via `composer update`