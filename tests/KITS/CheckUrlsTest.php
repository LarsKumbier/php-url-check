<?php


namespace KITS;


use KITS\Jobs\AsyncTest\OneHundredMsExec;

class CheckUrlsTest extends \PHPUnit_Framework_TestCase {
    /**
     * @group integration
     */
    public function test_CheckValidUrl()
    {
        $checkUrl = new CheckUrls();
        $checkUrl->addCheck('www.iana.org:80', '/', 5.0);
        $checkUrl->addCheck('www.iana.org:80', '/domains', 5.0);
        $checkUrl->addCheck('www.iana.org:80', '/numbers', 5.0);

        $jobCount = 3;

        $this->assertCount($jobCount, $checkUrl->getJobs());

        $checkUrl->exec();

        $results = $checkUrl->getResults();

        $this->assertCount($jobCount, $results);
        $this->assertCount($jobCount, $checkUrl->getJobs());

        foreach ($results as $id => $result)
            $this->assertEquals(\KITS\Jobs\ValidateUrl::RESULT_OK, $result, 'Got unexpected result for url '.$id.' - '.$result);
    }


    /**
     * @group integration
     */
    public function test_InvalidUrl()
    {
        $checkUrl = new CheckUrls();
        $checkUrl->addCheck('www.iana.org:80', '/a/b/c');
        $checkUrl->exec();
        $results = $checkUrl->getResults();
        $this->assertEquals(\KITS\Jobs\ValidateUrl::RESULT_CLIENT_ERROR, array_pop($results));
    }


    /**
     * @group timing
     */
    public function test_Throughput()
    {
        $urlCount = 50;
        $checkUrl = new CheckUrls();
        $checkUrl->setConcurrency(5);
        for ($i = 0; $i<$urlCount; $i++)
            $checkUrl->addCheck('www.iana.org:80', $i, 5.0);
        $checkUrl->exec();
        $this->assertEquals(0, $checkUrl->remainingJobs());
        $results = $checkUrl->getResults();
        $this->assertCount($urlCount, $results);

        $jobs = $checkUrl->getJobs();
        foreach ($jobs as $job)
            $this->assertFalse($job->hasTimedOut(), 'The Jobs should not time out.');
    }


    /**
     * @group unit
     */
    public function test_RedirectGeneratesNewJobs()
    {
        $jobs = array (
            'example.com:80/redirect' => new CheckUrlsTest\ValidateUrlFake(),
            'example.com/no-redirect' => new \KITS\Jobs\ValidateUrl('example.com', '/')
        );
        $checkUrl = new CheckUrls();
        $this->assertCount(0, $checkUrl->getJobs(), 'There should not be any jobs yet');
        $checkUrl->generateNewJobsForRedirects($jobs);
        $newJobs = $checkUrl->getJobs();

        $this->assertCount(1, $newJobs, 'There should be only one job for the redirection.');

        $redirectJob = array_pop($newJobs);
        $this->assertEquals(CheckUrlsTest\ValidateUrlFake::REDIRECT_HOST, $redirectJob->getHost());
        $this->assertEquals(CheckUrlsTest\ValidateUrlFake::REDIRECT_URL, $redirectJob->getUrl());
        $this->assertEquals(CheckUrlsTest\ValidateUrlFake::STATUS_NOT_STARTED, $redirectJob->getStatusCode(),
            'New Job should not have been started yet.');
    }


    /**
     * @group unit
     */
    public function test_CanRestartTimedoutJobs()
    {
        $checkUrls = new CheckUrls();
        $checkUrls->addJob(new OneHundredMsExec(1, 0.0001));
        $checkUrls->addJob(new OneHundredMsExec(1, 1.0));
        $checkUrls->exec();
        $this->assertCount(1, $checkUrls->getTimedoutJobs(), 'Arrange: There should be one timedout job');
        $this->assertCount(2, $checkUrls->getJobs(), 'Arrange: There should be two jobs in total - one timedout, one not');

        $restarted = $checkUrls->restartJobs(null, 1);
        $this->assertEquals(1, $restarted, 'One timedout job should have been restarted');
        $this->assertCount(3, $checkUrls->getJobs(), 'There should be three jobs in total after restarting one timedout');
        $checkUrls->exec();
        $this->assertCount(2, $checkUrls->getTimedoutJobs(), 'After restart and exec, there should be two timed out jobs');

        $restarted = $checkUrls->restartJobs(null, 1);
        $this->assertEquals(0, $restarted, 'MaxRestart should result in no more jobs being restarted');
    }
}
 