<?php


namespace KITS\Jobs\AsyncTest;


class OneHundredMsExec extends \KITS\Jobs\Async
{
    public $counter = 0;
    public $runTimes = 3;
    public $socket = null;

    public function __construct($id, $timeout = null)
    {
        $this->socket = stream_socket_client('tcp://google.de:80', $errno, $errstr, 30, STREAM_CLIENT_ASYNC_CONNECT);
        parent::__construct($id, $timeout);
    }

    protected function exec()
    {
        $this->counter++;
        usleep(100);

        if ($this->counter >= $this->runTimes)
            $this->finish();
    }

    public function callFinish()
    {
        return $this->finish();
    }

    public function callAddStatus($status)
    {
        return $this->addStatus($status);
    }

    public function callRemoveStatus($status)
    {
        return $this->removeStatus($status);
    }

    public function callTimeout()
    {
        return $this->timeout();
    }

    public function resetData()
    {
        $this->counter = 0;
        parent::resetData();
    }
}