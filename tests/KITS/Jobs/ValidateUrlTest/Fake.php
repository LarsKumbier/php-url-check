<?php


namespace KITS\Jobs\ValidateUrlTest;


class Fake extends \KITS\Jobs\ValidateUrl {
    public function callHandleResponse($response)
    {
        return $this->handleResponse($response);
    }

    public function getSocket()
    {
        return $this->sockets[0];
    }
}
 