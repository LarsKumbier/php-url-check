<?php


namespace KITS\Jobs;


class AsyncTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @group timing
     * @medium
     */
    public function test_DoesNotTimeout()
    {
        $fake = new AsyncTest\OneHundredMsExec(1, 1.0);
        $fake->runTimes = 1;
        while (!$fake->hasFinished()) {
            $fake->tick();
        }
        $this->assertFalse($fake->hasTimedOut(), 'Job should not have timed out.');
    }


    /**
     * @group timing
     * @medium
     */
    public function test_DoesTimeout()
    {
        $fake = new AsyncTest\OneHundredMsExec(1, 0.01);
        $fake->runTimes = 101;
        while (!$fake->hasFinished()) {
            $fake->tick();
        }
        $this->assertTrue($fake->hasTimedOut(), 'Job should have timed out.');
    }


    /**
     * @group unit
     */
    public function test_AsyncSavesTimestampsCorrectly()
    {
        $now = microtime(true);
        $fake = new AsyncTest\OneHundredMsExec(1, 2.0);
        $fake->runTimes = 10;
        while (!$fake->hasFinished()) {
            $fake->tick();
        }
        $this->assertGreaterThanOrEqual($now, $fake->getCreatedAt());
        $this->assertGreaterThanOrEqual($fake->getCreatedAt(), $fake->getStartedAt());
        $this->assertGreaterThanOrEqual($fake->getStartedAt(), $fake->getFinishedAt());
        $this->assertGreaterThanOrEqual($fake->getFinishedAt(), microtime(true));
    }


    /**
     * @group unit
     */
    public function test_StatusEngine()
    {
        $fake = new AsyncTest\OneHundredMsExec(1);
        $this->assertTrue($fake->hasStatus($fake::STATUS_NOT_STARTED));
        $this->assertFalse($fake->hasStatus($fake::STATUS_FINISHED));
        $fake->tick();
        $fake->callFinish();
        $this->assertFalse($fake->hasStatus($fake::STATUS_NOT_STARTED));
        $this->assertTrue($fake->hasStatus($fake::STATUS_FINISHED));

        // Adding a status twice should not change the result
        $fake->callAddStatus($fake::STATUS_FINISHED);
        $this->assertTrue($fake->hasStatus($fake::STATUS_FINISHED));

        // Removing a status twice should not change the result
        $fake->callRemoveStatus($fake::STATUS_FINISHED);
        $this->assertFalse($fake->hasStatus($fake::STATUS_FINISHED));
        $fake->callRemoveStatus($fake::STATUS_FINISHED);
        $this->assertFalse($fake->hasStatus($fake::STATUS_FINISHED));
    }


    /**
     * @group unit
     */
    public function test_CloneObjectShouldResetStatusAndMarkAsClone()
    {
        $obj = new AsyncTest\OneHundredMsExec(1);
        $obj->tick();
        $this->assertTrue($obj->hasStatus($obj::STATUS_STARTED));

        $clone = clone $obj;
        $this->assertTrue($clone->hasStatus($clone::STATUS_NOT_STARTED));
        $this->assertFalse($clone->hasStatus($clone::STATUS_STARTED));
        $this->assertTrue($clone->hasStatus($clone::STATUS_CLONE));
        $this->assertNull($clone->getFinishedAt());
        $this->assertNull($clone->getStartedAt());
        $this->assertGreaterThan($obj->getCreatedAt(), $clone->getCreatedAt());
    }
}
