<?php


namespace KITS\Jobs;


class ValidateUrlTest extends \PHPUnit_Framework_TestCase
{
    protected $fake = null;

    public function setUp()
    {
        $this->fake = new ValidateUrlTest\Fake('example.com:80', '/');
    }

    public function tearDown()
    {
        unset($this->fake);
    }


    /**
     * @dataProvider dp_ValidCodes
     * @group unit
     */
    public function test_ValidCodes($response)
    {
        $fake = $this->fake;
        $fake->callHandleResponse($response);
        $this->assertEquals($fake::RESULT_OK, $fake->getResult());
    }

    public function dp_ValidCodes()
    {
        return array (
            array("HTTP/1.0 200 OK\r\n"),
            array("HTTP/1.1 200 OK\r\n")
        );
    }


    /**
     * @dataProvider dp_RedirectCodes
     * @group unit
     */
    public function test_RedirectCodes($response)
    {
        $fake = $this->fake;
        $fake->callHandleResponse($response);
        $this->assertEquals($fake::RESULT_REDIRECT, $fake->getResult());
    }

    public function dp_RedirectCodes()
    {
        return array(
            array("HTTP/1.0 301 Moved Permanently\r\n"),
            array("HTTP/1.1 301 Moved\r\n"),
            array("HTTP/1.0 302 Found\r\n"),                // new url in "Location:"
            array("HTTP/1.1 303 See Other\r\n"),            // new url in "Location:"
            array("HTTP/1.1 307 Temporary Redirect\r\n")    // new url in "Location:"
        );
    }


    /**
     * @dataProvider dp_ClientErrorCodes
     * @group unit
     */
    public function test_ClientErrorCodes($response)
    {
        $fake = $this->fake;
        $fake->callHandleResponse($response);
        $this->assertEquals($fake::RESULT_CLIENT_ERROR, $fake->getResult());
    }

    public function dp_ClientErrorCodes()
    {
        return array(
            array("HTTP/1.0 400 Bad Request\r\n"),
            array("HTTP/1.1 401 Unauthorized\r\n"),
            array("HTTP/1.1 404 Not Found\r\n"),
            array("HTTP/1.1 408 Request Time-out\r\n"),
            array("HTTP/1.1 410 Gone\r\n")
        );
    }


    /**
     * @dataProvider dp_ServerErrorCodes
     * @group unit
     */
    public function test_ServerErrorCodes($response)
    {
        $fake = $this->fake;
        $fake->callHandleResponse($response);
        $this->assertEquals($fake::RESULT_SERVER_ERROR, $fake->getResult());
    }

    public function dp_ServerErrorCodes()
    {
        return array(
            array("HTTP/1.0 500 Internal Server Error\r\n"),
            array("HTTP/1.1 500 Internal Server Error\r\n"),
            array("HTTP/1.1 501 Not Implemented\r\n"),
            array("HTTP/1.1 502 Bad Gateway\r\n"),
            array("HTTP/1.1 503 Service Unavailable\r\n"),
            array("HTTP/1.1 504 Gateway Time-Out\r\n")
        );
    }

    /**
     * @group unit
     */
    public function test_CloningCreatesNewSocket()
    {
        $obj = new ValidateUrlTest\Fake('example.com:80', '/');
        $obj->tick();

        $clone = clone $obj;
        $this->assertTrue($clone->hasStatus($clone::STATUS_NOT_STARTED));
        $this->assertTrue($clone->hasStatus($clone::STATUS_CLONE));
        $this->assertNotEquals($obj->getSocket(), $clone->getSocket());
    }
}

