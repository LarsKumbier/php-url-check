<?php


namespace KITS\CheckUrlsTest;


class ValidateUrlFake extends \KITS\Jobs\ValidateUrl
{
    const REDIRECT_URL = '/redirect?ref=asdf#anchor';
    const REDIRECT_HOST = 'example.com';

    public function __construct() {
        $this->result = self::RESULT_REDIRECT;
        $this->serverResponse = "HTTP/1.1 301 Moved Permanently\r\n";
        $this->serverResponse .= "Via: 1.1 proxy.example.com\r\n";
        $this->serverResponse .= "Location: http://".self::REDIRECT_HOST.':80'.self::REDIRECT_URL."\r\n";
        $this->serverResponse .= "Content-Type: text/html; charset=iso-8859-1\r\n";
        $this->serverResponse .= "\r\n";
    }
}