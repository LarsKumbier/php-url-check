<?php


namespace KITS;

class JobsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @group unit
     */
    public function test_DoesRunAJob()
    {
        $jobEngine = new Jobs();
        $jobEngine->addJob(new JobsTest\Fake(15));

        $jobEngine->exec();

        $jobs = $jobEngine->getJobs();
        $this->assertCount(1, $jobs);
        $job = array_pop($jobs);
        $this->assertGreaterThan(0, $job->getCounter());
    }

    /**
     * @group unit
     */
    public function test_ConcurrencyIsHandled()
    {
        $concurrency = 10;
        $jobEngine = new Jobs();

        $jobEngine->setConcurrency($concurrency);

        for ($i = 0; $i<$concurrency*10; $i++)
            $jobEngine->addJob(new JobsTest\Fake($i, 1.0));

        $this->assertCount($concurrency*10, $jobEngine->getRemainingJobs());
        $this->assertCount(0, $jobEngine->getActiveJobs());

        while($jobEngine->remainingJobs() > 0) {
            $jobEngine->tick();
            $this->assertLessThanOrEqual($concurrency, $jobEngine->activeJobs());
        }
    }


    /**
     * @group unit
     */
    public function test_CannotAddSimilarJobsIfForbidden()
    {
        $this->setExpectedException('InvalidArgumentException');
        $jobEngine = new Jobs();
        $jobEngine->addJob(new JobsTest\Fake(23), true);
        $jobEngine->addJob(new JobsTest\Fake(23), true);
    }


    /**
     * @group unit
     */
    public function test_CanAddSimilarJobsIfAllowed()
    {
        $jobEngine = new Jobs();
        $jobEngine->addJob(new JobsTest\Fake(23), true);
        $jobEngine->addJob(new JobsTest\Fake(23), false);
        $this->assertCount(2, $jobEngine->getJobs());
    }
}