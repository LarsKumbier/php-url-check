<?php


namespace KITS\JobsTest;


class Fake extends \KITS\Jobs\Async
{
    protected $counter = 0;

    protected function exec()
    {
        $this->counter++;
        if ($this->counter == 10)
            $this->finish();
    }

    public function getCounter()
    {
        return $this->counter;
    }
}