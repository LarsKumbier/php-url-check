<?php

namespace KITS;

use KITS\Jobs\Async;

/**
 * Class CheckUrls
 *
 * @package KITS
 * @author  Lars Kumbier <lars@kumbier.it>
 * @license GPL-3.0
 */
class CheckUrls extends Jobs
{
    protected $urls = array();

    protected $timeoutsSinceLastOverloadChange = 0;
    protected $overloadThreshold = 10;
    protected $lastOverloadChange = 0.0;
    protected $overloadReductionStep = 2;

    protected $minimumConcurrency = 5;

    const OVERLOAD_TRESHOLD_INFINITE = null;



    public function setOverloadThreshold($new)
    {
        if ((!is_int($new) || $new < 0) && $new !== self::OVERLOAD_TRESHOLD_INFINITE)
            throw new \InvalidArgumentException('Must be 0+ or TIMEOUT_TRESHOLD_INFINITE');
    }

    public function getTimeoutTreshold()
    {
        return $this->overloadThreshold;
    }


    public function setMinimumConcurrency($new)
    {
        if (!is_int($new) || $new < 1)
            throw new \InvalidArgumentException('Must be a positive integer above 0');
        $this->minimumConcurrency = $new;
    }

    public function getMinimumConcurrency()
    {
        return $this->minimumConcurrency;
    }

    public function addCheck($host, $url, $timeout = null)
    {
        $timeout = $timeout === null ? (float)$this->defaultTimeout : $timeout;

        $job = new \KITS\Jobs\ValidateUrl($host, $url, $timeout);

        if (!$this->hasJob($job->getId()))
            $this->addJob($job);

        return $this;
    }


    public function generateNewJobsForRedirects($jobs = null)
    {
        if ($jobs === null)
            $jobs = $this->getJobs();

        foreach ($jobs as $job) {
            if ($job->getResult() == $job::RESULT_REDIRECT) {
                $serverResponse = $job->getServerResponse();
                $location = $this->getLocationFromServerResponse($serverResponse);
                $urlparts = parse_url($location);

                if ($urlparts === false)
                    continue;

                $host = array_key_exists('host', $urlparts) ? $urlparts['host'] : $job->getHost();
                $host .= ':' . (array_key_exists('port', $urlparts) ? $urlparts['port'] : $job->getPort());

                $path = $this->getPathFromUrl($location);

                if (empty($path))
                    continue;
                $timeout = $job->getTimeout();

                $this->addCheck($host, $path, $timeout);
            }
        }

        return $this;
    }


    protected function checkOverloading($timedoutJobs)
    {
        $jobs = $this->filterJobsWithFinishingTimeYoungerThan($this->lastOverloadChange, $timedoutJobs);
        $this->timeoutsSinceLastOverloadChange += count($jobs);
        if ($this->timeoutsSinceLastOverloadChange > $this->overloadThreshold)
            $this->reactToOverloading($this->overloadReductionStep);
    }


    protected function reactToOverloading($change)
    {
        $concurrency = $this->getConcurrency();
        $new = max($concurrency - $change, $this->getMinimumConcurrency());
        $this->setConcurrency($new);

        $this->lastOverloadChange = microtime(true);
        $this->timeoutsSinceLastOverloadChange = 0;
    }


    protected function getPathFromUrl($url)
    {
        $urlparts = parse_url($url);
        $path = array_key_exists('path', $urlparts) ? $urlparts['path'] : '';
        $path .= array_key_exists('query', $urlparts) ? '?'.$urlparts['query'] : '';
        $path .= array_key_exists('fragment', $urlparts) ? '#'.$urlparts['fragment'] : '';
        return $path;
    }


    protected function getLocationFromServerResponse($response)
    {
        $found = preg_match('/\nLocation: (.*)\r\n/i', $response, $matches);
        if ($found === 0)
            return false;
        return $matches[1];
    }


    public function getResults()
    {
        $responses = array();
        foreach ($this->getJobs() as $id => $job) {
            $responses[$id] = $job->getResult();
        }
        return $responses;
    }
}