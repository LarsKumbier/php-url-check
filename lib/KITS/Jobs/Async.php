<?php


namespace KITS\Jobs;


/**
 * Class Async
 *
 * @package KITS\Jobs
 * @author  Lars Kumbier <lars@kumbier.it>
 * @license GPL-3.0
 *
 * @todo    Bitmask operations should be a trait
 * @todo    Bitmasks should be defined as incremental numbers instead of bits
 */
abstract class Async
{
    const TIMEOUT_INFINITE = -1.0;
    const TIMEOUT_DEFAULT  = 30.0;

    const STATUS_NOT_STARTED =  1;
    const STATUS_STARTED     =  2;
    const STATUS_FINISHED    =  4;
    const STATUS_TIMEDOUT    =  8;
    const STATUS_CLONE       = 16;

    protected $timeout = self::TIMEOUT_DEFAULT;
    protected $status  = self::STATUS_NOT_STARTED;

    protected $id;
    protected $createdAt;
    protected $startedAt;
    protected $finishedAt;

    protected $errors = array();


    abstract protected function exec();


    public function __construct($id, $timeout = null)
    {
        $this->setId($id);
        $this->setTimeout($timeout);
        $this->createdAt = microtime(true);
    }


    protected function addError($message, $code = null)
    {
        $this->errors[] = new \Exception($message, $code);
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function hasErrors()
    {
        return !empty($this->errors);
    }


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        if (!is_string($id) && !is_int($id))
            throw new \InvalidArgumentException('id has to be human-readable (string or integer).');
        $this->id = $id;
        return $this;
    }



    public function getTimeout()
    {
        return $this->timeout;
    }

    public function setTimeout($timeout)
    {
        if (!is_float($timeout) && !is_null($timeout))
            throw new \InvalidArgumentException('timeout has to be a float value or null, if no timeout given.');

        if (is_null($timeout))
            $timeout = self::TIMEOUT_DEFAULT;

        $this->timeout = $timeout;

        return $this;
    }


    public function getStatusCode()
    {
        return $this->status;
    }


    public function hasTimedOut()
    {
        return $this->hasStatus(self::STATUS_TIMEDOUT);
    }


    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    public function getStartedAt()
    {
        return $this->startedAt;
    }


    public function getFinishedAt()
    {
        return $this->finishedAt;
    }


    public function hasFinished()
    {
        return $this->hasStatus(self::STATUS_FINISHED);
    }


    public function tick()
    {
        if ($this->hasFinished())
            return $this;

        if (!$this->hasStatus(self::STATUS_STARTED))
            $this->start();

        $this->handleTimeout();
        $this->exec();

        return $this;
    }

    protected function validateStatusCode($status)
    {
        if (!is_int($status))
            throw new \InvalidArgumentException('Status should be an integer from this class\' STATUS_* constants.');
    }

    public function hasStatus($status)
    {
        $this->validateStatusCode($status);
        return (bool)($this->getStatusCode() & $status);
    }


    protected function addStatus($status)
    {
        $this->validateStatusCode($status);
        if ($this->hasStatus($status))
            return $this;
        $this->status |= $status;
    }


    protected function removeStatus($status)
    {
        $this->validateStatusCode($status);
        if (!$this->hasStatus($status))
            return $this;
        $this->status &= ~$status | 0;
    }

    protected function setStatus($status)
    {
        $this->validateStatusCode($status);
        $this->status = $status;
    }

    protected function handleTimeout()
    {
        if ($this->timeout === self::TIMEOUT_INFINITE)
            return;

        $now = microtime(true);

        if ($now - $this->startedAt > $this->timeout)
            $this->timedout();
    }

    protected function start()
    {
        $this->removeStatus(self::STATUS_NOT_STARTED);
        $this->addStatus(self::STATUS_STARTED);
        $this->startedAt = microtime(true);
    }

    protected function finish()
    {
        $this->removeStatus(self::STATUS_STARTED);
        $this->addStatus(self::STATUS_FINISHED);
        $this->finishedAt = microtime(true);
    }

    protected function timedout()
    {
        $this->addStatus(self::STATUS_TIMEDOUT);
        $this->finish();
    }

    public function resetData()
    {
        $isClone = $this->hasStatus(self::STATUS_CLONE);
        $this->setStatus(self::STATUS_NOT_STARTED);
        $this->createdAt = microtime(true);
        $this->startedAt = null;
        $this->finishedAt = null;
        if ($isClone)
            $this->addStatus(self::STATUS_CLONE);
    }

    public function __clone()
    {
        $this->addStatus(self::STATUS_CLONE);
        $this->resetData();
    }

    public function __toString()
    {
        return (string)$this->getId();
    }
}