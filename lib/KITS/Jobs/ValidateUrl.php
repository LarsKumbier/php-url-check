<?php


namespace KITS\Jobs;


/**
 * Class ValidateUrl
 *
 * @package KITS\Jobs
 * @author  Lars Kumbier <lars@kumbier.it>
 * @license GPL-3.0
 */
class ValidateUrl extends Async
{
    const DEFAULT_PORT = 80;

    protected $host;
    protected $port = self::DEFAULT_PORT;
    protected $url;
    protected $sockets;
    protected $socketErrorCode;
    protected $socketErrorString;

    protected $serverResponse = "";

    protected $socketStatus = 1;
    const SOCKETSTATUS_OPEN = 1;
    const SOCKETSTATUS_WAIT_FOR_RESPONSE = 2;


    protected $result = 0;
    const RESULT_UNKNOWN = 0;
    const RESULT_OK = 2;
    const RESULT_REDIRECT = 3;
    const RESULT_CLIENT_ERROR = 4;
    const RESULT_SERVER_ERROR = 5;


    public function __construct($host, $url, $timeout = null)
    {
        $this->setHost($host);
        $this->setUrl($url);

        $id = $this->getHost().$this->getUrl();

        parent::__construct($id, $timeout);

        $this->openSocket($this->getHost().':'.$this->getPort());

        return $this;
    }


    public function __destruct()
    {
        $this->deleteSocket();
    }


    public function getResult()
    {
        return $this->result;
    }


    public function getServerResponse()
    {
        return $this->serverResponse;
    }


    /**
     * Sets a Hostname or IP-Address, with or without portnumber
     * @param $host
     * @return $this
     * @example example.com, example.com:80, www.example.com:80, 1.2.3.4, 1.2.3.4:80
     */
    public function setHost($host)
    {
        $port = self::DEFAULT_PORT;
        if (preg_match('/(.*):(\d{1,5})$/', $host, $matches)) {
            $host = $matches[1];
            $port = $matches[2];
        }

        $this->host = $host;
        $this->port = $port;
        return $this;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getPort()
    {
        return $this->port;
    }


    protected function setUrl($url)
    {
        if (substr($url, 0, 1) !== '/')
            $url = '/'.$url;

        $this->url = $url;
    }

    public function getUrl()
    {
        return $this->url;
    }


    protected function exec()
    {
        $readSockets = $writeSockets = $this->sockets;

        $todoCount = stream_select($readSockets, $writeSockets, $exceptions = null, $this->timeout);

        if ($todoCount === 0)
            return $this;

        if (!empty($writeSockets))
            $this->handleWriteSockets($writeSockets);

        if (!empty($readSockets))
            $this->handleReadSockets($readSockets);
    }


    protected function handleWriteSockets($sockets)
    {
        if ($this->socketStatus == self::SOCKETSTATUS_WAIT_FOR_RESPONSE)
            return;

        $message = "HEAD ".$this->getUrl()." HTTP/1.1\r\n";
        $message .= "Host: ".$this->getHost()."\r\n";
        $message .= "\r\n";

        if (@fwrite($sockets[0], $message) === false) {
            $this->addError('Could not send message to '.$this->getId().', aborting job.');
            $this->deleteSocket();
            echo $this->getServerResponse();
            $this->finish();
            return;
        }

        $this->socketStatus = self::SOCKETSTATUS_WAIT_FOR_RESPONSE;
    }


    protected function handleReadSockets($sockets)
    {
        if ($this->socketStatus !== self::SOCKETSTATUS_WAIT_FOR_RESPONSE)
            return;

        $response = fread($sockets[0], 8192);

        if (strlen($response) == 0) {
            if ($this->socketStatus == self::SOCKETSTATUS_WAIT_FOR_RESPONSE) {
                $this->handleResponse($this->serverResponse);
            }
            $this->deleteSocket();
            $this->finish();
        } else {
            $this->serverResponse .= $response;
        }
    }


    protected function handleResponse($response)
    {
        $pattern = ('/HTTP\/1.\d (\d)\d\d/i');
        $occurrences = preg_match($pattern, $response, $matches);

        if (empty($occurrences)) {
            $this->addError('Could not find a valid status code in response: '.$response);
            return;
        }

        switch ($matches[1]) {
            case "2":
                $this->result = self::RESULT_OK;
                break;

            case "3":
                $this->result = self::RESULT_REDIRECT;
                break;

            case "4":
                $this->result = self::RESULT_CLIENT_ERROR;
                break;

            case "5":
                $this->result = self::RESULT_SERVER_ERROR;
                break;

            default:
                $this->addError('Unknown Status Code found for url '.$this->getHost().$this->getUrl().': '.$matches[0]);
        }
    }


    protected function openSocket($host)
    {
        $socket = stream_socket_client("tcp://$host", $this->socketErrorCode, $this->socketErrorString, $this->timeout,
            STREAM_CLIENT_ASYNC_CONNECT);

        if ($socket === false) {
            $this->addError($this->socketErrorString, $this->socketErrorCode);
            $this->finish();
            return false;
        }

        $this->sockets = array($socket);
    }


    protected function deleteSocket()
    {
        if ($this->sockets === null)
            return;

        if (array_key_exists(0, $this->sockets)) {
            if (get_resource_type($this->sockets[0]) === 'stream') {
                fclose($this->sockets[0]);
            }
            unset($this->sockets[0]);
        }
    }


    public function __clone()
    {
        $unixHost = $this->getHost().':'.$this->getPort();
        if ($this->openSocket($unixHost) === false)
            throw new \Exception('Coult not create a new socket to '.$unixHost.' - error ('.$this->socketErrorCode.') '.$this->socketErrorString);

        parent::__clone();
    }
}