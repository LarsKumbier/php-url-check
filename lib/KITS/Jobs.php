<?php


namespace KITS;

use KITS\Jobs\Async;

/**
 * Class Jobs
 *
 * @package KITS
 * @author  Lars Kumbier <lars@kumbier.it>
 * @license GPL-3.0
 */
class Jobs 
{
    protected $defaultTimeout = 30.0;
    protected $jobs = array();
    protected $concurrency = null;

    const CONCURRENCY_UNLIMITED = null;

    const MAXRESTART_INIFINITE = null;

    const COUNT_INFINITE = null;


    public function setConcurrency($limit)
    {
        $exceptionMsg = 'Concurrency limit has to be 1+ or CONCURRENCY_UNLIMITED';

        if (!is_int($limit) && $limit !== self::CONCURRENCY_UNLIMITED)
            throw new \InvalidArgumentException($exceptionMsg);

        if (is_int($limit) && $limit <= 0)
            throw new \InvalidArgumentException($exceptionMsg);

        $this->concurrency = $limit;
        return $this;
    }


    public function getConcurrency()
    {
        return $this->concurrency;
    }


    /**
     * Runs all registered jobs, until no jobs are remaining - blocking!
     */
    public function exec()
    {
        while ($this->remainingJobs($this->jobs) > 0) {
            $this->tick();
        }
    }


    /**
     * Calls each registered job once and returns immediately afterwards - non-blocking!
     */
    public function tick()
    {
        $this->startNewJobs();
        foreach ($this->getActiveJobs() as $id => $job) {
            $job->tick();
        }
    }


    protected function startNewJobs()
    {
        $concurrency = $this->getConcurrency();

        if (is_null($concurrency))
            $concurrency = $this->unstartedJobs();

        while($this->activeJobs() < $concurrency && $this->unstartedJobs() > 0) {
            $jobs = $this->getUnstartedJobs(null, 1);
            $nextJob = array_pop($jobs);
            $nextJob->tick();
        }
    }


    /**
     * @param  Jobs\Async $newJob
     * @param  bool       $failOnExistingJobId
     * @throws \Exception
     * @todo   first parameter should be an interface
     */
    public function addJob(Jobs\Async $newJob, $failOnExistingJobId = false)
    {
        $jobId = $newJob->getId();

        if ($this->hasJob($jobId)) {
            if ($failOnExistingJobId) {
                throw new \InvalidArgumentException('Job of id '.$jobId.' already exists.');
            }
            $jobId = $this->generateNonexistingJobid($jobId);
        }

        if ($newJob->getTimeout() === Jobs\Async::TIMEOUT_DEFAULT)
            $newJob->setTimeout($this->defaultTimeout);

        $this->jobs[$jobId] = $newJob;
    }

    protected function generateNonexistingJobid($jobId)
    {
        $newId = $jobId;
        $postfix = 0;
        while ($this->hasJob($newId.$postfix)) {
            $postfix++;
        }
        return $newId.$postfix;
    }

    public function getJobs()
    {
        return $this->jobs;
    }


    public function hasJob($jobId)
    {
        return array_key_exists($jobId, $this->jobs);
    }


    public function deleteJob($jobId)
    {
        if ($this->hasJob($jobId)) {
            unset($this->jobs[$jobId]);
            return true;
        }
        return false;
    }


    public function getRemainingJobs($jobs = null, $count = null)
    {
        return $this->filterJobs($jobs, $count, Jobs\Async::STATUS_NOT_STARTED | Jobs\Async::STATUS_STARTED);
    }

    public function remainingJobs($jobs = null)
    {
        return count($this->getRemainingJobs($jobs));
    }


    public function getActiveJobs($jobs = null, $count = null)
    {
        return $this->filterJobs($jobs, $count, Jobs\Async::STATUS_STARTED);
    }

    public function activeJobs($jobs = null)
    {
        return count($this->getActiveJobs($jobs));
    }


    public function getUnstartedJobs($jobs = null, $count = null)
    {
        return $this->filterJobs($jobs, $count, Jobs\Async::STATUS_NOT_STARTED);
    }

    public function unstartedJobs($jobs = null)
    {
        return count($this->getUnstartedJobs($jobs));
    }


    public function getTimedoutJobs($jobs = null, $count = null)
    {
        return $this->filterJobs($jobs, $count, Jobs\Async::STATUS_TIMEDOUT);
    }

    public function timedoutJobs($jobs = null)
    {
        return count($this->getTimedoutJobs($jobs));
    }


    public function getFinishedJobs($jobs = null, $count = null)
    {
        return $this->filterJobs($jobs, $count, Jobs\Async::STATUS_FINISHED);
    }

    public function finishedJobs($jobs = null)
    {
        return count($this->getFinishedJobs($jobs));
    }


    public function filterJobs($jobs = null, $count = null, $status)
    {
        if (is_null($jobs))
            $jobs = $this->getJobs();

        $result = array();
        foreach ($jobs as $id => $job) {
            if ($job->hasStatus($status)) {
                $result[$id] = $job;
                if (!is_null($count) && count($result) >= $count) {
                    break;
                }
            }
        }

        return $result;
    }

    public function hasClones($jobId)
    {
        $clones = $this->filterJobs(null, null, Async::STATUS_CLONE);
        foreach ($clones as $clone)
            if ($clone->getId() == $jobId)
                return true;
        return false;
    }

    public function getClones($jobId)
    {
        $jobs = $this->getJobs();

        $clones = $this->filterJobs($jobs, null, Async::STATUS_CLONE);
        $result = array();
        foreach ($clones as $cloneId => $clone)
            if ($clone->getId() == $jobId)
                $result[$cloneId] = $clone;
        return $result;
    }




    /**
     * @param  null $jobs
     * @param  int  $maxRestart
     * @return int  How many jobs have been restarted
     */
    public function restartJobs($jobs = null, $maxRestart = 1)
    {
        if ($jobs === null)
            $jobs = $this->getTimedoutJobs();

        if ((!is_int($maxRestart) && $maxRestart !== self::MAXRESTART_INIFINITE) || is_int($maxRestart) && $maxRestart < 0)
            throw new \InvalidArgumentException('$maxRestart must be 0+ or MAXRESTART_INIFINITE (for inifite)');

        $count = 0;
        foreach ($jobs as $job) {
            $clones = $this->getClones($job->getId());
            if ($maxRestart !== self::MAXRESTART_INIFINITE && count($clones) >= $maxRestart)
                continue;

            $this->addJob(clone $job);
            $count++;
        }

        return $count;
    }


    /**
     * @param float       $timestamp
     * @param null|array  $jobs
     * @param null|int    $count
     * @return array
     */
    public function filterJobsWithFinishingTimeYoungerThan($timestamp, $jobs = null, $count = self::COUNT_INFINITE)
    {
        if (!is_float($timestamp))
            throw new \InvalidArgumentException('Timestamp is supposed to be float [e.g. created by `microtime(true)`]');

        if ($jobs === null)
            $jobs = $this->getFinishedJobs();

        $result = array();
        foreach ($jobs as $jobId => $job)
            if ($job->getFinishedAt() > $timestamp)
                $result[$jobId] = $job;

        return $result;
    }
} 